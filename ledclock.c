/*
 * ledclock.c
 *
 * Martin Brown
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>

#include <linux/sched.h>
#include <linux/kernel.h>	/* printk() */
#include <linux/fs.h>		/* everything... */
#include <linux/errno.h>	/* error codes */
#include <linux/slab.h>
#include <linux/types.h>        /* size_t */
#include <asm/uaccess.h>
#include <linux/mm.h>
#include <linux/ioport.h>
#include <linux/wait.h>
#include <linux/timer.h>
#include <linux/mutex.h>
#include <linux/spinlock.h>
#include <linux/wait.h>

#include "ledclock.h"

struct ledclock_dev *dev;

/* default is the first printer port on PC's. "ledclock_base" is there too
   because it's what we want to use in the code */

                                     /* 0     1     2     3     4     5     6     7     8     9     */
//unsigned char ledclock_lookuptable[] = {0x7B, 0x42, 0x37, 0x57, 0x4E, 0x5D, 0x7D, 0x43, 0xFF, 0x4F};
unsigned char temp_lookuptable[] =       {0x7B, 0x42, 0x37, 0x57, 0x4E, 0x5D, 0x7D, 0x43, 0xFF, 0x4F};
unsigned char * ledclock_lookuptable;

/*
 * all of the parameters have no "ledclock_" prefix, to save typing when
 * specifying them at load time
 */
static int major = 0;	/* dynamic by default */
module_param(major, int, 0);

static unsigned long base = 0x378;
unsigned long ledclock_base = 0;

module_param(base, long, 0);

static unsigned int modulus = LEDCLOCK_DEFAULT_MOD;
module_param(modulus, int, 0);

static unsigned short wrap = LEDCLOCK_DEFAULT_WRAP;
module_param(wrap, short, 0);

static unsigned long speed = LEDCLOCK_DEFAULT_SPEED;
module_param(speed, long, 0);

static unsigned long dwell = LEDCLOCK_DEFAULT_DWELL;
module_param(dwell, long, 0);

static unsigned long pause = LEDCLOCK_DEFAULT_PAUSE;
module_param(pause, long, 0);

MODULE_AUTHOR ("Martin Brown");
MODULE_LICENSE("Dual BSD/GPL");

/*
 * The devices with low minor numbers write/read burst of data to/from
 * specific I/O ports (by default the parallel ones).
 * 
 * The device with 128 as minor number returns ascii strings telling
 * when interrupts have been received. Writing to the device toggles
 * 00/FF on the parallel data lines. If there is a loopback wire, this
 * generates interrupts.  
 */

int ledclock_init(void)
{
	int result;

	/*
	 * first, sort out the base/ledclock_base ambiguity: we'd better
	 * use ledclock_base in the code, for clarity, but allow setting
	 * just "base" at load time. Same for "irq".
	 */
	ledclock_base = base;

        printk(KERN_ALERT "ledclock_init\n");

	/* Get our needed resources. */
	if (! request_region(ledclock_base, LEDCLOCK_NR_PORTS, "ledclock")) {
		printk(KERN_INFO "ledclock: can't get I/O port address 0x%lx\n",
				ledclock_base);
		return -ENODEV;
	}

	/* Here we register our device - should not fail thereafter */
	result = register_chrdev(major, "ledclock", &ledclock_fops);
	if (result < 0) {
		printk(KERN_INFO "ledclock: can't get major number\n");
		release_region(ledclock_base,LEDCLOCK_NR_PORTS);  /* FIXME - use-mem case? */
		return result;
	}

	dev = kmalloc(sizeof(*dev), GFP_KERNEL);

	if (dev == NULL) {
                //unregister_chrdev_region(major);
                return -ENOMEM;
        }

	memset(dev, 0, sizeof(*dev));

	init_timer(&dev->timer);

	ledclock_lookuptable = temp_lookuptable;

	dev->state = LEDCLOCK_STATE_UNINITIALIZED;

	spin_lock_init(&dev->spinlock);
	mutex_init(&dev->mut);

	init_waitqueue_head(&dev->wait);
	init_waitqueue_head(&dev->timerq);

	dev->modulus = modulus;
//	dev->tdelay = speed;
//	dev->pause = pause;
	ledclock_setspeed(speed);
	ledclock_setpause(pause);
	ledclock_setdwell(dwell);
	ledclock_setwrap(wrap);

	dev->curtick = 0;
	dev->timer.data = (unsigned long) dev;
	dev->timer.function = ledclock_timer_fn;

	if (major == 0) major = result; /* dynamic */

	return 0;
}

int ledclock_open (struct inode *inode, struct file *filp)
{
        if (mutex_lock_interruptible(&dev->mut))
                return -ERESTARTSYS;

	dev->port = ledclock_getport(inode);

	mutex_unlock(&dev->mut);

	return 0;
}

/* first, the port-oriented device */

ssize_t do_ledclock_read (struct inode *inode, struct file *filp, char __user *buf,
		size_t count, loff_t *f_pos)
{

        unsigned char *kbuf;
	int temp_curtick = dev->curtick;

        if (mutex_lock_interruptible(&dev->mut))
                return -ERESTARTSYS;

	if(dev->state == LEDCLOCK_STATE_UNINITIALIZED || dev->state == LEDCLOCK_STATE_INITIALIZED)
		temp_curtick = 0;

	else if(dev->state == LEDCLOCK_STATE_DISPLAYING_HI || dev->state == LEDCLOCK_STATE_DISPLAYING_PAUSE)
		temp_curtick = dev->curtick;

	else if(dev->curtick == 0)
		temp_curtick = dev->modulus - 1;

	else
		temp_curtick = dev->curtick - 1;

	if(temp_curtick < 0)
		temp_curtick = 0;

	if(count != sizeof(unsigned int))
	{
		mutex_unlock(&dev->mut);
		return -EINVAL;
	}

        kbuf = (unsigned char*) &(temp_curtick);

	if(copy_to_user(buf, kbuf, sizeof(unsigned int)))
	{
		mutex_unlock(&dev->mut);
		return -EFAULT;
	}

	mutex_unlock(&dev->mut);

	return sizeof(unsigned int);
}


/*
 * Version-specific methods for the fops structure.  FIXME don't need anymore.
 */
ssize_t ledclock_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
{
        printk(KERN_ALERT "ledclock_read\n");

	return do_ledclock_read(filp->f_dentry->d_inode, filp, buf, count, f_pos);
}

ssize_t do_ledclock_write (struct inode *inode, struct file *filp, const char __user *buf,
		size_t count, loff_t *f_pos)
{
	int retval = count;

 	unsigned char *kbuf;
	unsigned int mod;

        if (mutex_lock_interruptible(&dev->mut))
                return -ERESTARTSYS;

	if(count != sizeof(unsigned int))
	{
		mutex_unlock(&dev->mut);
		return -EINVAL;
	}

	kbuf = kmalloc(count, GFP_KERNEL);

        if (!kbuf)
	{
		mutex_unlock(&dev->mut);
                return -ENOMEM;
	}

        if (copy_from_user(kbuf, buf, count))
	{
		mutex_unlock(&dev->mut);
		kfree(kbuf);
                return -EFAULT;
	}

        mod = (unsigned int) *kbuf;

	if(mod < 0)
		mod = LEDCLOCK_DEFAULT_MOD;

        kfree(kbuf);

	/* fill the dev for our timer function */
	dev->modulus = mod;

	mutex_unlock(&dev->mut);

	ledclock_restart();

        return retval;
}


ssize_t ledclock_write(struct file *filp, const char __user *buf, size_t count,
		loff_t *f_pos)
{
	return do_ledclock_write(filp->f_dentry->d_inode, filp, buf, count, f_pos);
}

int ledclock_release (struct inode *inode, struct file *filp)
{
	return 0;
}

void ledclock_cleanup(void)
{
        printk(KERN_ALERT "ledclock cleanup\n");

	if(dev)
	{
		if(timer_pending(&dev->timer))
			del_timer_sync(&dev->timer);

		dev->state = LEDCLOCK_STATE_UNINITIALIZED;

		kfree(dev);
	}

	dev->in_timer_fn = 0;
	wake_up_interruptible(&dev->wait);
	wake_up_interruptible(&dev->timerq);

        outb((unsigned char) 0x00, dev->port);
	smp_wmb();

	unregister_chrdev(major, "ledclock");

	release_region(ledclock_base,LEDCLOCK_NR_PORTS);
}

long ledclock_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{

        int err = 0;
        int retval = 0;
	unsigned char *kbuf;

        /*
         * extract the type and number bitfields, and don't decode
         * wrong cmds: return ENOTTY (inappropriate ioctl) before access_ok()
         */
        if (_IOC_TYPE(cmd) != LEDCLOCK_IOC_MAGIC) return -ENOTTY;
        if (_IOC_NR(cmd) > LEDCLOCK_IOC_MAXNR) return -ENOTTY;

        /*
         * the direction is a bitmask, and VERIFY_WRITE catches R/W
         * transfers. `Type' is user-oriented, while
         * access_ok is kernel-oriented, so the concept of "read" and
         * "write" is reversed
         */
        if (_IOC_DIR(cmd) & _IOC_READ)
                err = !access_ok(VERIFY_WRITE, (void __user *)arg, _IOC_SIZE(cmd));
        else if (_IOC_DIR(cmd) & _IOC_WRITE)
                err =  !access_ok(VERIFY_READ, (void __user *)arg, _IOC_SIZE(cmd));
        if (err) return -EFAULT;

        switch(cmd) {

	case LEDCLOCK_IOCSHOW:

		retval = ledclock_show();

	break;

	case LEDCLOCK_IOCPAUSE:

		retval = ledclock_pause();

	break;

	case LEDCLOCK_IOCPAUSESET:

 		retval = ledclock_setpause(arg);

 	break;

	case LEDCLOCK_IOCRESET:

		retval = ledclock_reset();

	break;

	case LEDCLOCK_IOCRESTART:

		retval = ledclock_restart();

	break;

	case LEDCLOCK_IOCDWELLSET:

		retval = ledclock_setdwell(arg);

	break;

	case LEDCLOCK_IOCWRAPSET:

		retval = ledclock_setwrap(arg);

	break;

	case LEDCLOCK_IOCSPEEDSET:

		retval = ledclock_setspeed(arg);

	break;

	case LEDCLOCK_IOCTABLESET:

		//retval = __get_user(ledclock_lookuptable, (unsigned char *  __user *)arg);

		kbuf = kmalloc(sizeof(unsigned int) * 10, GFP_KERNEL);

		if(!kbuf)
			return -ENOMEM;

		if(ledclock_lookuptable != NULL)
		{
			if(copy_from_user(kbuf, (char __user *) arg, sizeof(unsigned int) * 10))
			{
				kfree(kbuf);
				return -EFAULT;
			}

			ledclock_lookuptable = kbuf;
		}

	break;

        }

        return retval;
}

int ledclock_show(void)
{
	if(wait_event_interruptible(dev->timerq, !dev->in_timer_fn))
		return -ERESTARTSYS;

        if (mutex_lock_interruptible(&dev->mut))
                return -ERESTARTSYS;

	if(dev->state == LEDCLOCK_STATE_UNINITIALIZED)
	{
		dev->state = LEDCLOCK_STATE_INITIALIZED;
		ledclock_outb(0);
	}

	mutex_unlock(&dev->mut);

	return 0;
}

int ledclock_start()
{
	if(wait_event_interruptible(dev->timerq, !dev->in_timer_fn))
		return -ERESTARTSYS;

        if (mutex_lock_interruptible(&dev->mut))
                return -ERESTARTSYS;

	if(!timer_pending(&dev->timer))
	{
		unsigned long j = jiffies;
		dev->timer.expires = j + dev->tdelay; /* parameter */
		del_timer(&dev->timer);
		dev->in_timer_fn = 0;
		wake_up_interruptible(&dev->timerq);
		wake_up_interruptible(&dev->wait);
		add_timer(&dev->timer); /* register the timer */

		dev->state = LEDCLOCK_STATE_TICKING;
	}

	mutex_unlock(&dev->mut);

	return 0;
}

int ledclock_reset(void)
{

	if(wait_event_interruptible(dev->timerq, !dev->in_timer_fn))
		return -ERESTARTSYS;

        if (mutex_lock_interruptible(&dev->mut))
                return -ERESTARTSYS;


	if(timer_pending(&dev->timer))
	{
		del_timer_sync(&dev->timer);
	        smp_wmb();
	}

	dev->curtick = 0;

	dev->state = LEDCLOCK_STATE_UNINITIALIZED;

	ledclock_outb(-1);

        mutex_unlock(&dev->mut);

	return 0;
}

int ledclock_pause(void)
{
	if(wait_event_interruptible(dev->timerq, !dev->in_timer_fn))
		return -ERESTARTSYS;

	if(wait_event_interruptible/*_timeout*/(
						dev->wait,
						dev->state != LEDCLOCK_STATE_DISPLAYING_HI
							   && dev->state != LEDCLOCK_STATE_DISPLAYING_PAUSE
							   && dev/*,
						dev->tdelay + dev->pdelay + dev->dwell*/
					   ))
		return -ERESTARTSYS;

	if(timer_pending(&dev->timer))	// if its running, stop the timer but don't affect the display
	{
	        if (mutex_lock_interruptible(&dev->mut))
        	        return -ERESTARTSYS;

		del_timer_sync(&dev->timer);
		dev->state = LEDCLOCK_STATE_PAUSED;
		ledclock_printstate();

		mutex_unlock(&dev->mut);
	}
	else if(dev->state == LEDCLOCK_STATE_PAUSED)// if it's not running, assume it is paused and resume it
	{
		ledclock_start();
	}

	return 0;
}

int ledclock_setpause(int pause)
{
        if (mutex_lock_interruptible(&dev->mut))
                return -ERESTARTSYS;

	dev->pdelay = pause * HZ/1000;

	mutex_unlock(&dev->mut);

	return 0;
}

int ledclock_restart(void)
{
        if (mutex_lock_interruptible(&dev->mut))
                return -ERESTARTSYS;

	dev->curtick = 0;

	mutex_unlock(&dev->mut);

	ledclock_start();

	return 0;
}

int ledclock_setdwell(int dwell)
{
        if (mutex_lock_interruptible(&dev->mut))
                return -ERESTARTSYS;

	dev->dwell = dwell * HZ/1000;

	mutex_unlock(&dev->mut);

	return 0;
}

int ledclock_setwrap(int wrap)
{
        if (mutex_lock_interruptible(&dev->mut))
                return -ERESTARTSYS;

	dev->wrap = wrap;

	mutex_unlock(&dev->mut);

	return 0;
}

int ledclock_setspeed(int speed)
{
        if (mutex_lock_interruptible(&dev->mut))
                return -ERESTARTSYS;

	dev->tdelay = speed * HZ/1000;

	mutex_unlock(&dev->mut);

	return 0;
}

unsigned long ledclock_getport(struct inode *inode)
{
	int minor = iminor(inode);
        return ledclock_base + (minor&0x0f);
}

void ledclock_outb(int digit)
{
	unsigned char hdigit;
	if(digit == -1)
		hdigit = LEDCLOCK_BLANK;
	else
		hdigit = ledclock_lookuptable[digit];

	outb(hdigit, dev->port);
	smp_wmb();

	ledclock_printstate();
}

void ledclock_timer_fn(unsigned long arg)
{
    struct ledclock_dev *dev = (struct ledclock_dev *)arg;
    unsigned long j = jiffies;

    dev->in_timer_fn = 1;

    // LEDCLOCK_STATE_DISPLAYING should be the initial state if starting from 0
    // it's not displaying a multi-digit number yet so this should be false until
    // we get to 10
    if(dev->state != LEDCLOCK_STATE_DISPLAYING_HI && dev->state != LEDCLOCK_STATE_DISPLAYING_PAUSE)
    {
        dev->state = LEDCLOCK_STATE_DISPLAYING;
    }

    // if this is a multi-digit number, then let's do something different
    if(ledclock_numdigits(dev->curtick) > 1)
    {
        // if we're displaying the first digit of the multi-digit number
	if(dev->state == LEDCLOCK_STATE_DISPLAYING)
	{
            // initialize the curdigit index to the first leftmost digit
            dev->curdigitindex = ledclock_numdigits(dev->curtick) - 1;

            // change the state to displaying the digit of a multi-digit number
            dev->state = LEDCLOCK_STATE_DISPLAYING_HI;
	}

        dev->prevjiffies = j;

        // if we're displaying the digit of a multi-digit number
	if(dev->state == LEDCLOCK_STATE_DISPLAYING_HI)
	{
                // display the number, calculate how much time elapsed due to the dwell time
                // of displaying the number and set the timer to expire for the dwell time duration
		ledclock_outb(ledclock_nthdigit(dev->curtick, dev->curdigitindex));
	        dev->elapsed_jiffies += (dev->dwell);
		dev->timer.expires += dev->dwell;

                // now that we've displayed the digit of a multi-digit number,
                // we may need to display a pause between digits if its not the last digit
                // checking whether or not its the last digit happens further down
		dev->state = LEDCLOCK_STATE_DISPLAYING_PAUSE;
	}

        // if we're displaying the pause between digits
	else if(dev->state == LEDCLOCK_STATE_DISPLAYING_PAUSE)
	{
                // display a blank pause, then calculate how much time elapsed due to the pause time
                // of the blank pause
		ledclock_outb(-1);

                // since the pause comes after the digit, in order to display the next digit
                // we need to change the index to it
		dev->curdigitindex--;
	        dev->elapsed_jiffies += (dev->pdelay);
		dev->timer.expires += dev->pdelay;

                // we're not displaying the pause anymore, we need to move on to the next digit
		dev->state = LEDCLOCK_STATE_DISPLAYING_HI;
	}

	// set the timer again if it's not displaying the last digit,
	// then exit this routine after setting the timer so that we can
        // either display another digit or a pause
	if(dev->curdigitindex != -1)
        {
            add_timer(&dev->timer);
	    dev->in_timer_fn = 0;
	    wake_up_interruptible(&dev->timerq);
            return;
        }

	// fix previous false additionn since timer was not set
	// dev->elapsed_jiffies -= dev->pdelay;

        // if we've displayed the last digit, then change the state,
        // no need to display a pause
        dev->state = LEDCLOCK_STATE_DISPLAYING;

	// wake up the pause function
	wake_up_interruptible(&dev->wait);
    }

    // if this is a single digit number, then display the number
    else
    {
        ledclock_outb(dev->curtick);
    }

    dev->state = LEDCLOCK_STATE_TICKING;

    // if elapsed jiffies from displaying dwell and pauses accumulated to
    // at least a second, increment the ticks accordingly, otherwise
    // just increment the tick by 1
    if((dev->elapsed_jiffies / HZ) != 0)
    {
	dev->curtick += (int) (dev->elapsed_jiffies / HZ);

        // since we used some jiffies from the elapsed time, decrement them
        // so that we don't use them again. there may still be some left over however
	dev->elapsed_jiffies -= ( (int) dev->elapsed_jiffies / HZ) * HZ;
    }
    else
    {
	dev->curtick++;
    }

    // if we've gone pass the modulus
    if(dev->curtick >= dev->modulus)
    {
       // make sure to display the correct wrapped number
       dev->curtick = dev->curtick - dev->modulus;

       // but if wrapping is turned off then change the state, and leave
       // this routine so that the timer is not set again
       if(!dev->wrap)
       {
          dev->state = LEDCLOCK_STATE_STOPPED;
//          dev->state = LEDCLOCK_STATE_INITIALIZED;
//	  dev->curtick = 0;
          ledclock_printstate();

//	  mutex_unlock(&dev->timer);
	  dev->in_timer_fn = 0;
	  wake_up_interruptible(&dev->timerq);
	  wake_up_interruptible(&dev->wait);
          return;
       }
    }

    // if displaying a single digit number, make sure to use the elapsed jiffies
    // left over from displaying multi-digit numbers, then reset elapsed jiffies
    // so they aren't used again for the next number
    if(dev->curtick < 10)
    {
        dev->timer.expires = dev->timer.expires + dev->tdelay - dev->elapsed_jiffies;
        dev->elapsed_jiffies = 0;
    }
    else
    {
        dev->timer.expires += dev->tdelay;

        // need to consider this delay also when changing numbers
	dev->elapsed_jiffies += dev->tdelay;
    }

    dev->prevjiffies = j;
    add_timer(&dev->timer);

    dev->in_timer_fn = 0;
    wake_up_interruptible(&dev->timerq);
    wake_up_interruptible(&dev->wait);
//    mutex_unlock(&dev->timer);
}

int ledclock_numdigits(int number)
{
    int num = 0;

    while(number != 0)
    {
        number /= 10;
        num++;
    }

    return num;
}

/**
 * Source: http://stackoverflow.com/questions/203854/how-to-get-the-nth-digit-of-an-integer-with-bit-wise-operations
 */
int ledclock_nthdigit(int digit, int n)
{
    char cdigit;

    while (n--) {
        digit /= 10;
    }

    cdigit = (digit % 10) + '0';

    return ((int) cdigit) - 48;
}

void ledclock_printstate(void)
{
	printk(KERN_ALERT "tick=%d\tmod=%d\tspeed=%luHZ\tpause=%luHZ\tdwell=%luHZ\twrap=%s\tstate=%s\n",
			dev->curtick,
//			ledclock_nthdigit(dev->curtick, dev->curdigitindex),
			dev->modulus,
			dev->tdelay,
			dev->pdelay,
			dev->dwell,
			dev->wrap ? "yes" : "no ",
			ledclock_states_ch[dev->state]);
}

module_init(ledclock_init);
module_exit(ledclock_cleanup);
