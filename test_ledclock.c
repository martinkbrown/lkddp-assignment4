#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include "ledclock.h"

#define LEDCLOCK_WRITE    "--write"
#define LEDCLOCK_READ     "--read"
#define LEDCLOCK_SHOW	  "--show"
#define LEDCLOCK_PAUSE    "--pause"
#define LEDCLOCK_RESET    "--reset"
#define LEDCLOCK_RESTART  "--restart"
#define LEDCLOCK_DWELLSET "--dwell"
#define LEDCLOCK_WRAPSET  "--wrap"
#define LEDCLOCK_SPEEDSET "--speed"
#define LEDCLOCK_TABLESET "--table"

void usage()
{
	printf("\n\tUsage: ./test [%s modulus] | [%s] | [%s] | [%s [millis]] | [%s] | [%s] | [%s millis] | [%s 0|1] | [%s millis] | [%s]\n\n",
		LEDCLOCK_WRITE, LEDCLOCK_READ, LEDCLOCK_SHOW, LEDCLOCK_PAUSE, LEDCLOCK_RESET,
		LEDCLOCK_RESTART, LEDCLOCK_DWELLSET, LEDCLOCK_WRAPSET, LEDCLOCK_SPEEDSET, LEDCLOCK_TABLESET);

	exit(-1);
}

int main(int argc, char ** argv) {

   int fd, result;
   unsigned int t;
   size_t sizeofuint = sizeof(unsigned int);
   int i;

   if(argc == 1) usage();

   if ((fd = open ("/dev/ledclock", O_RDWR)) == -1) {
      perror("opening file");
      return -1;
   }

   if(strcmp(argv[1], LEDCLOCK_WRITE) == 0)
   {

	if(argc != 3) usage();

	t = atoi(argv[2]);

   	if ((result = write (fd, &t, sizeofuint)) != sizeofuint)
	{
		perror("writing");
		return -1;
	}
   }

   else if(strcmp(argv[1], LEDCLOCK_READ) == 0)
   {
	if ((result = read (fd, &t, sizeofuint)) != sizeofuint)
	{
		perror("writing");
		return -1;
	}

	printf("%d\n", t);
   }

   else if(strcmp(argv[1], LEDCLOCK_SHOW) == 0)
	result = ioctl(fd, LEDCLOCK_IOCSHOW);

   else if(strcmp(argv[1], LEDCLOCK_PAUSE) == 0)
   {
        if(argc == 2)
		result = ioctl(fd, LEDCLOCK_IOCPAUSE);
	else if(argc == 3)
		result = ioctl(fd, LEDCLOCK_IOCPAUSESET, atoi(argv[2]));
	else usage();
   }

   else if(strcmp(argv[1], LEDCLOCK_RESET) == 0)
	result = ioctl(fd, LEDCLOCK_IOCRESET);

   else if(strcmp(argv[1], LEDCLOCK_RESTART) == 0)
	result = ioctl(fd, LEDCLOCK_IOCRESTART);

   else if(strcmp(argv[1], LEDCLOCK_DWELLSET) == 0)
   {
        if(argc != 3) usage();

	result = ioctl(fd, LEDCLOCK_IOCDWELLSET, atoi(argv[2]));
   }

   else if(strcmp(argv[1], LEDCLOCK_WRAPSET) == 0)
   {
        if(argc != 3) usage();

	result = ioctl(fd, LEDCLOCK_IOCWRAPSET, atoi(argv[2]));
   }

   else if(strcmp(argv[1], LEDCLOCK_SPEEDSET) == 0)
   {
        if(argc != 3) usage();

	result = ioctl(fd, LEDCLOCK_IOCSPEEDSET, atoi(argv[2]));
   }

   else if(strcmp(argv[1], LEDCLOCK_TABLESET) == 0)
   {
	unsigned char * lookuptable;
	lookuptable = malloc(sizeof(unsigned char) * 10);
	unsigned char temp[] = {0x4F, 0xFF, 0x43, 0x7D, 0x5D, 0x4E, 0x57, 0x37, 0x42, 0x7B};

	for(i = 0; i < 10; i++)
		lookuptable[i] = temp[i];

	result = ioctl(fd, LEDCLOCK_IOCTABLESET, lookuptable);
   }

   close(fd);

   return 0;
}
