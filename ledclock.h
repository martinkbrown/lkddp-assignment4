/*
 * ledclock.h
 *
 */

#ifndef _LEDCLOCK_H_
#define _LEDCLOCK_H_

#include <linux/ioctl.h> /* needed for the _IOW etc stuff used later */

#ifdef __KERNEL__

#define LEDCLOCK_NR_PORTS	8       /* use 8 ports by default */
#define LEDCLOCK_BLANK          0x00    /* to blank the LEDs */

#define LEDCLOCK_DEFAULT_MOD	10      /* default modulus */
#define LEDCLOCK_DEFAULT_WRAP   1       /* wrap or dont wrap? */
#define LEDCLOCK_DEFAULT_SPEED  ((HZ * 1000) / HZ)
#define LEDCLOCK_DEFAULT_PAUSE  ((LEDCLOCK_DEFAULT_SPEED)/4)
#define LEDCLOCK_DEFAULT_DWELL  ((LEDCLOCK_DEFAULT_SPEED)/2)

enum ledclock_states {LEDCLOCK_STATE_UNINITIALIZED=0, LEDCLOCK_STATE_INITIALIZED, LEDCLOCK_STATE_TICKING,
	LEDCLOCK_STATE_DISPLAYING, LEDCLOCK_STATE_DISPLAYING_HI, LEDCLOCK_STATE_DISPLAYING_PAUSE,
        LEDCLOCK_STATE_STOPPED, LEDCLOCK_STATE_PAUSED};

char * ledclock_states_ch[] = {"UNINITIALIZED", "INITIALIZED", "TICKING", "DISPLAYING", "DISPLAYING_HI", "DISPLAYING_PAUSE", "STOPPED", "PAUSED"};

/* This data structure used as "data" for the timer and tasklet functions */
struct ledclock_dev
{
        struct timer_list timer;
        unsigned long prevjiffies;
	unsigned long elapsed_jiffies;
        unsigned long port;
        int state;

        unsigned int modulus;
        unsigned int curtick;
	unsigned int curdigitindex;

	unsigned short wrap;
	unsigned long tdelay;
	unsigned long dwell;
	unsigned long pdelay;

	unsigned short in_timer_fn;

        struct mutex mut;

	spinlock_t spinlock;

	wait_queue_head_t wait;
	wait_queue_head_t timerq;
};

int ledclock_init(void);
int ledclock_open (struct inode *inode, struct file *filp);
ssize_t ledclock_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos);
ssize_t ledclock_write(struct file *filp, const char __user *buf, size_t count, loff_t *f_pos);
int ledclock_release (struct inode *inode, struct file *filp);
void ledclock_cleanup (void);
long ledclock_ioctl(struct file *filp, unsigned int cmd, unsigned long arg);

/* ioctls */
int ledclock_show(void);
int ledclock_start(void);
int ledclock_reset(void);
int ledclock_pause(void);
int ledclock_setpause(int pause);
int ledclock_restart(void);
int ledclock_setwrap(int wrap);
int ledclock_setspeed(int speed);
int ledclock_setdwell(int dwell);
int ledclock_setpause(int pause);

/* custom functions */
unsigned long ledclock_getport(struct inode *inode);
void ledclock_outb(int digit);
void ledclock_timer_fn(unsigned long arg);
int ledclock_numdigits(int number);
int ledclock_nthdigit(int digit, int n);
void ledclock_printstate(void);

struct file_operations ledclock_fops = {
        .owner		= THIS_MODULE,
        .read		= ledclock_read,
        .write		= ledclock_write,
        .open		= ledclock_open,
        .release	= ledclock_release,
	.unlocked_ioctl	= ledclock_ioctl
};

#endif

/*
 * Ioctl definitions
 */

/* Use '-' as magic number */
#define LEDCLOCK_IOC_MAGIC  '-'
/* Please use a different 8-bit number in your code */


/*
 * S means "Set" through a ptr,
 * T means "Tell" directly with the argument value
 * G means "Get": reply by setting through a pointer
 * Q means "Query": response is on the return value
 * X means "eXchange": switch G and S atomically
 * H means "sHift": switch T and Q atomically
 */
#define LEDCLOCK_IOCSHOW         _IO(LEDCLOCK_IOC_MAGIC,    0)
#define LEDCLOCK_IOCRESET        _IO(LEDCLOCK_IOC_MAGIC,    1)
#define LEDCLOCK_IOCPAUSE        _IO(LEDCLOCK_IOC_MAGIC,    2)
#define LEDCLOCK_IOCPAUSESET     _IOW(LEDCLOCK_IOC_MAGIC,   3, int)
#define LEDCLOCK_IOCRESTART      _IO(LEDCLOCK_IOC_MAGIC,    4)
#define LEDCLOCK_IOCDWELLSET     _IOW(LEDCLOCK_IOC_MAGIC,   5, int)
#define LEDCLOCK_IOCWRAPSET      _IOW(LEDCLOCK_IOC_MAGIC,   6, int)
#define LEDCLOCK_IOCSPEEDSET     _IOW(LEDCLOCK_IOC_MAGIC,   7, int)
#define LEDCLOCK_IOCTABLESET     _IOW(LEDCLOCK_IOC_MAGIC,   8, int)
#define LEDCLOCK_IOC_MAXNR                                  8

#endif /* _LEDCLOCK_H_ */
